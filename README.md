# Steem Notifier

You can run from shell by using:

``` bash
node index.js <nameofaccount>
```

<center> OR </center>

``` bash
node index.js
// The above defaults the account name to `tipu` (a busy account that aids testing of new transfers)
```


Name of account can be any steem username,you can test with the following accounts:
- `tipu` (because it's a bot account that runs steady transfer)
- `bittrex` (very busy too)
- `deepcrypto8`  for Binance Exchange(quite busy too)

You can also run without 
Example:

```bash
node index.js
```

```bash
node index.js bittrex
```

```bash
node index.js bittrex
```

> FOr now, it doesn't run automatically, I am just previewing, I am trying to make the best decision for the node process.

Tell me what you think 